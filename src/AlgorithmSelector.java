public class AlgorithmSelector {

    private double min;
    private double max;
    private int selector;

    public AlgorithmSelector(int selector) {
        this.selector = selector;
        algorithmSelector();
    }

    public void algorithmSelector() {
        if (selector == 1) {
            min = -510;
            max = 510;
        } else if (selector == 2) {
            min = -2.048;
            max = 2.048;
        } else {
            min = -5.12;
            max = 5.12;
        }
    }

    // -510 < x < 510
    public double schwefelFunction(double[] chromosome) {
        double sum = 0;
        double a = 418.982887;
        for (int i = 0; i < chromosome.length; i++) {
            sum += (-chromosome[i] * Math.sin(Math.sqrt(Math.abs(i)))) + (a * (i + 1));
        }
        return sum;
    }

    // -2.048 < x < 2.048
    public double rosenbrockFunction(double[] chromosome) {
        double sum = 0;
        for (int i = 0; i < (chromosome.length - 1); i++) {
            sum += 100 * (((chromosome[i] * chromosome[i]) - chromosome[i + 1])
                    * (chromosome[i] * chromosome[i]) - chromosome[i + 1])
                    + ((1 - chromosome[i]) * (1 - chromosome[i]));
        }
        return sum;
    }

    // -5.12 < x < 5.12
    public double rastriginFunction(double[] chromosome) {
        double sum = 0;
        for (int i = 0; i < chromosome.length; i++) {
            sum += (chromosome[i] * chromosome[i]) - (10 * Math.cos(2 * Math.PI * chromosome[i]));
        }
        sum = (10 * chromosome.length) + sum;
        return sum;
    }

    public double getMin() {
        return min;
    }

    public void setMin(double min) {
        this.min = min;
    }

    public double getMax() {
        return max;
    }

    public void setMax(double max) {
        this.max = max;
    }

    public int getSelector() {
        return selector;
    }

    public void setSelector(int selector) {
        this.selector = selector;
    }
}

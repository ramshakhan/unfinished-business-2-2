public class Individual {

    private double[] chromosome;
    private int selector;

    public Individual(double[] chromosome, int selector) {
        this.chromosome = chromosome;
        this.selector = selector;
    }

//    // Evaluate the Individual with the Equation
//    public double individualEvaluation() {
//        double sum = 0;
//        for (int i = 0; i < chromosome.length; i++) {
//            sum += ((i + 1) * (chromosome[i] * chromosome[i]));
//        }
//        return sum;
//    }

    // Evaluate the Individual with the Equation
    public double individualEvaluation() {
        AlgorithmSelector algorithmSelector = new AlgorithmSelector(selector);
        double sum;
        if (selector == 1) {
            sum = algorithmSelector.schwefelFunction(chromosome);
        } else if (selector == 2) {
            sum = algorithmSelector.rosenbrockFunction(chromosome);
        } else {
            sum = algorithmSelector.rastriginFunction(chromosome);
        }
        return sum;
    }

    public double[] getChromosome() {
        return chromosome;
    }
}